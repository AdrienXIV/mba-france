import { Component } from "react";
import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";

export default class SiteMap extends Component {
  static async getInitialProps({ req, res }: { req: NextApiRequest; res: NextApiResponse }) {
    if (res) {
      const { data } = await axios.get(`${process.env.NEXT_PUBLIC_HOST}/api/sitemap`);

      res.setHeader("Content-Type", "text/xml");
      res.write(data);
      res.end();
    }
  }
}
