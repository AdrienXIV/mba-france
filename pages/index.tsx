import Image from "next/image";
import styles from "@styles/Home.module.css";
import React, { useRef, useState } from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import Lottie from "lottie-react";
import animationEmail from "@animations/send-email.json";
import animationCode from "@animations/code-animation.json";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReCAPTCHA, { ReCAPTCHAProps } from "react-google-recaptcha";

const Home = () => {
  const recaptchaRef = useRef<{ props: any }>(null);
  const [state, setState] = useState({
    email: "",
    nom: "",
    prenom: "",
    message: "",
    sujet: "",
    captcha: false,
  });
  const [contactSuccess, setContactSuccess] = useState(false);
  const [contactLoad, setContactLoad] = useState(false);

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    toast.clearWaitingQueue();

    // si le Captcha n'est pas coché
    if (!state.captcha) {
      return toast("Veuillez valider le Captcha", { type: "error", position: "top-center", delay: 1000 });
    }

    setContactLoad(true);
    try {
      const { data } = await axios.post("/api/contact", state);
      setContactSuccess(true);
      setState({
        email: "",
        nom: "",
        prenom: "",
        message: "",
        sujet: "",
        captcha: false,
      });
      // on remet à 0 le Captcha
      recaptchaRef.current && recaptchaRef.current.props.grecaptcha.reset();
    } catch (error) {
      Array.isArray(error.response.data)
        ? error.response.data.map((err: string) => {
            toast(err, { type: "warning", position: "top-center", delay: 1000 });
          })
        : toast(error.response.data?.error || "Erreur serveur", { type: "error", position: "top-center", delay: 1000 });
    }
    setContactLoad(false);
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const name = e.target.name;
    const value = e.target.value;

    setState(prev => ({ ...prev, [name]: value }));
  };

  return (
    <>
      <div className={styles.container}>
        <Image priority layout="fill" objectFit="cover" src="/img/2.jpg" quality={100} />
      </div>
      <main className={styles.main}>
        <h1>Le site MBA FRANCE MAINTENANCE est en construction</h1>
        <h2>Merci de nous envoyer votre message</h2>

        <Container fluid className={styles.contact}>
          <Row>
            <Col className={styles.contactTitle} sm>
              Adresse
            </Col>
            <Col className={styles.contactTitle} sm>
              Téléphone
            </Col>
            <Col className={styles.contactTitle} sm>
              Courriel
            </Col>
          </Row>
          <Row>
            <Col className={styles.contactSubtitle} sm>
              154 Allée des Erables, Villepinte
            </Col>
            <Col className={styles.contactSubtitle} sm>
              <a href="tel:0952450235">+33952450235</a>
            </Col>
            <Col className={styles.contactSubtitle} sm>
              <a href="mailto:contact@mbafrancemaintenance.com">contact@mbafrancemaintenance.com</a>
            </Col>
          </Row>

          <Container fluid className={styles.formContainer}>
            <Form onSubmit={onSubmit}>
              {!contactSuccess && (
                <>
                  <div style={{ textAlign: "end" }}>
                    <span>*</span> : Champs requis
                  </div>
                  <Form.Group>
                    <Row>
                      <Col>
                        <Form.Label>
                          Prénom <span>*</span>
                        </Form.Label>
                        <Form.Control onChange={onChange} required value={state.prenom} name="prenom" type="text" placeholder="Votre prénom" />
                      </Col>
                      <Col>
                        <Form.Label>
                          Nom <span>*</span>
                        </Form.Label>
                        <Form.Control onChange={onChange} required value={state.nom} name="nom" type="text" placeholder="Votre nom" />
                      </Col>
                    </Row>
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>
                      Courriel <span>*</span>
                    </Form.Label>
                    <Form.Control onChange={onChange} required value={state.email} name="email" type="email" placeholder="Votre courriel" />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>
                      Sujet <span>*</span>
                    </Form.Label>

                    <Form.Control onChange={onChange} required value={state.sujet} name="sujet" type="text" placeholder="Sujet de votre demande" />
                    <div style={{ textAlign: "end" }}>
                      <small>{state.sujet.length} /255</small>
                    </div>
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>
                      Message <span>*</span>
                    </Form.Label>

                    <Form.Control
                      onChange={onChange}
                      required
                      value={state.message}
                      name="message"
                      as="textarea"
                      rows={7}
                      placeholder="Votre message"
                    />
                    <div style={{ textAlign: "end" }}>
                      <small>{state.message.length} /4000</small>
                    </div>
                  </Form.Group>

                  <ReCAPTCHA
                    ref={recaptchaRef as any}
                    sitekey={process.env.NEXT_PUBLIC_CAPTCHA as string}
                    onChange={e => {
                      setState(prev => ({ ...prev, captcha: true }));
                    }}
                  />
                  {!state.captcha && <div className={styles.captchaValidation}>Veuillez valider le Captcha</div>}
                </>
              )}

              {contactSuccess ? (
                <Lottie
                  alt="Animation indiquant la bonne récéption du courriel"
                  animationData={animationEmail}
                  autoPlay
                  loop={false}
                  style={{ width: 100, margin: "0 auto" }}
                />
              ) : (
                <div style={{ textAlign: "center", marginTop: 25 }}>
                  {contactLoad ? (
                    <Loader type="TailSpin" color="#ff3440" height={50} width={50} />
                  ) : (
                    <Button
                      variant="primary"
                      disabled={!state.captcha}
                      type="submit"
                      style={{
                        opacity: state.captcha ? 1 : 0.5,
                        cursor: state.captcha ? "pointer" : "not-allowed",
                      }}
                    >
                      Envoyer
                    </Button>
                  )}
                </div>
              )}
            </Form>
          </Container>
        </Container>
      </main>
    </>
  );
};

export default Home;
