import style from "@styles/Footer.module.css";

const Footer = () => {
  return (
    <footer className={style.footer}>
      <iframe
        className={style.youtube}
        src="https://www.youtube-nocookie.com/embed/Vo7MlKzAWxA?autoplay=1&mute=1"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        // webkitallowfullscreen="true"
        // mozallowfullscreen="true"
      ></iframe>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.25s">
            <p>© Copyright 2021 Dev Agency. All Rights Reserved</p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
