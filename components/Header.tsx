import Link from "next/link";

const Header = () => {
  return (
    <nav className="fixed-top navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <Link href="/" passHref>
          <a className="text-dark logo">
            Dev <span> Agency </span>
          </a>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          {/* <div className="navbar-nav">
            <div className="col-md-3">
              <a className="nav-link active" aria-current="page" href="#">
                <Link
                  href={{
                    pathname: "/",
                    hash: "top",
                  }}
                  passHref
                >
                  Accueil
                </Link>
              </a>
            </div>
            <div className="col-md-3">
              <a className="nav-link active" aria-current="page" href="#">
                <Link
                  href={{
                    pathname: "/",
                    hash: "top",
                  }}
                  passHref
                >
                  Accueil
                </Link>
              </a>
            </div>
            <div className="col-md-3">
              <a className="nav-link active" aria-current="page" href="#">
                <Link
                  href={{
                    pathname: "/",
                    hash: "top",
                  }}
                  passHref
                >
                  Accueil
                </Link>
              </a>
            </div>
            <div className="col-md-3">
              <a className="nav-link active" aria-current="page" >
                <Link
                  href={{
                    pathname: "/",
                    hash: "top",
                  }}
                  passHref
                >
                  Accueil
                </Link>
              </a>
            </div>
          </div> */}
        </div>
      </div>
    </nav>
  );
};

export default Header;
